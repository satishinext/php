#!/bin/bash -xvf

DATE=`date+%Y-%m-%d`
cd /mnt/vol1/Refresh_backup
find . -maxdepth 1 -mtime +1 -type d -ls | xargs rm -rf

find -newermt "$DATE 00:00" ! -newermt "$DATE 12:00" |xargs rm -rf
